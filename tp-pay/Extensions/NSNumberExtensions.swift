//
//  NSNumberExtensions.swift
//  tp-pay
//
//  Created by Preston Farr on 10/4/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import Foundation

extension NSNumber {
    var maskCardStringValue: String { return String(format: "XXXX XXXX XXXX %i", self.intValue % 10000) }
}
