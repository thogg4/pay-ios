//
//  UIViewControllerExtensions.swift
//  tp-pay
//
//  Created by Preston Farr on 10/4/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import UIKit

extension UIViewController {
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
