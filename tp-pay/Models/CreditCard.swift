//
//  CreditCard.swift
//  tp-pay
//
//  Created by Preston Farr on 10/4/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import Foundation

public enum CardType{
    static let visa = { return "Visa" }
    static let mastercard = { return "Mastercard" }
    static let amex = { return "American Express" }
    static let discover = { return "Discover" }
}

public struct CreditCard {
    public var alias: String
    public var cardholderName: String
    public var lastFour: String
    public var cardType: String
    public var expDate: Date
    public var billPhone: NSNumber
    
    init() {
        alias = ""
        cardholderName = ""
        lastFour = "4444"
        cardType = ""
        expDate = Date()
        billPhone = 0
    }
}
