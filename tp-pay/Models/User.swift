//
//  User.swift
//  tp-pay
//
//  Created by Preston Farr on 10/4/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import Foundation

public struct User : Codable {
    var id: String
    var email: String
    //var address: Address
    //let lastLogin: Date
}
