//
//  Address.swift
//  tp-pay
//
//  Created by Preston Farr on 10/4/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import Foundation

public struct Address: Codable {
    var street: String
    var street2: String
    var city: String
    var state: String
    var country: String
}
