//
//  AccountViewController.swift
//  tp-pay
//
//  Created by Preston Farr on 10/2/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import UIKit
import Eureka

class AccountViewController: FormViewController {
    private var cardInfo: CreditCard = CreditCard()
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        retrieveUserData()
    }
    
    func retrieveUserData() {
        let userId = UserDefaults.standard.string(forKey: "userId")
        let urlString = "https://murmuring-fortress-75115.herokuapp.com/customers/\(userId!)"
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("hzHoUx04yGR4FBWZPcu9Kw", forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                self.renderError()
                return
            }
            
            let jsonDecoder = JSONDecoder()
            self.user = try! jsonDecoder.decode(User.self, from: responseData)
            self.renderForm()
        }.resume()
    }
    
    func renderError() {
        DispatchQueue.main.sync {
            let alert = UIAlertController(title: "Error", message: "We got an error while getting your info", preferredStyle: .alert)
            var tryAgain = UIAlertAction(title: "Try Again", style: .default) { (_) in self.retrieveUserData() }
            var signOut = UIAlertAction(title: "Sign Out", style: .destructive) { (_) in self.appDelegate.signOut() }
            alert.addAction(tryAgain)
            alert.addAction(signOut)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func renderForm() {
        DispatchQueue.main.sync {
            self.form +++ Section("Profile")
                <<< TextRow() { row in
                    row.title = "Email"
                    row.value = self.user!.email
                    row.baseCell.isUserInteractionEnabled = false
                }
                
                +++ Section("Billing")
                <<< TextRow() { row in
                    row.title = "Full Name"
                    row.value = self.cardInfo.cardholderName
                    row.baseCell.isUserInteractionEnabled = false
                }
                <<< TextRow() { row in
                    row.title = "Card"
                    row.value = self.cardInfo.lastFour
                    row.baseCell.isUserInteractionEnabled = false
                }
                <<< DateRow() { row in
                    row.title = "Exp"
                    row.value = self.cardInfo.expDate
                    row.dateFormatter?.dateFormat = "MM/yyyy"
                    row.baseCell.isUserInteractionEnabled = false
                }
                <<< TextRow() { row in
                    row.title = "Type"
                    row.value = self.cardInfo.cardType
                    row.baseCell.isUserInteractionEnabled = false
                }
                <<< ButtonRow() { row in
                    row.title = "Edit Card"
                    row.onCellSelection(self.buttonEditCardTapped)
                }
                <<< ButtonRow() { row in
                    row.title = "Remove Card"
                    row.baseCell.tintColor = UIColor.red
                    row.onCellSelection(self.buttonRemoveCardTapped)
                }
                
                +++ Section("")
                <<< ButtonRow() { row in
                    row.title = "Sign Out"
                    row.onCellSelection(self.buttonSignOutTapped)
                }
                
                +++ Section(" ") // For extra aesthetic space at bottom.
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var _ : UIViewController = segue.destination as UIViewController
    }
    
    func buttonEditCardTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
//        self.performSegue(withIdentifier: "showMainTabBar", sender: self)
    }
    
    func buttonRemoveCardTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        // TODO - clear the data
        
        let alert = UIAlertController(title: "Are You Sure?", message: "This action will remove your default card information permanently.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Remove", style: UIAlertActionStyle.destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func buttonSignOutTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        self.appDelegate.signOut()
    }
}

