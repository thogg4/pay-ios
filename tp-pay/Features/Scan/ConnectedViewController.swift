//
//  ConnectedViewController.swift
//  tp-pay
//
//  Created by Tim on 10/28/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import UIKit
import SVProgressHUD

class ConnectedViewController: UIViewController {
    @IBOutlet weak var connectedToLabel: UILabel!
    
    var registerId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        SVProgressHUD.show(withStatus: "Connecting...")
        
        print(registerId)
        
        let userId = UserDefaults.standard.string(forKey: "userId")
        let urlString = "https://murmuring-fortress-75115.herokuapp.com/customers/\(userId!)/connect_to_register"
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "PATCH"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("hzHoUx04yGR4FBWZPcu9Kw", forHTTPHeaderField: "Authorization")
        request.httpBody = try? JSONSerialization.data(withJSONObject: ["register_id": registerId])
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            let parsedResult: [String: AnyObject]!
            parsedResult = try! JSONSerialization.jsonObject(with: responseData) as! [String: AnyObject]
            
            if error != nil {
                print("error")
                print(error!.localizedDescription)
            } else {
                print(parsedResult)
                if parsedResult["error"] != nil {
                    print("error")
                    DispatchQueue.main.sync { SVProgressHUD.dismiss() }
                    let alert = UIAlertController(title: "Couldn't find that register", message: "Please scan again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default) { (_) in self.performSegue(withIdentifier: "unwindToScan", sender: self) })
                    self.present(alert, animated: true, completion: nil)
                } else {
                    DispatchQueue.main.sync {
                        SVProgressHUD.dismiss()
                        let name = parsedResult["connected_register"]!["name"]
                        self.connectedToLabel.text = name as! String
                    }
                }
            }
        }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

