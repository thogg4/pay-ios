//
//  MainTabBarController.swift
//  tp-pay
//
//  Created by Preston Farr on 10/3/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func signOut() {
        let alert = UIAlertController(title: "Sign Out", message: "Just to show it works", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Sign Out", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.selectedViewController?.present(alert, animated: true, completion: nil)
    }
}
