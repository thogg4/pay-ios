//
//  LoginViewController.swift
//  tp-pay
//
//  Created by Preston Farr on 10/2/17.
//  Copyright © 2017 Preston Farr. All rights reserved.
//

import UIKit
import Eureka
import SVProgressHUD

class LoginViewController: FormViewController {
    @IBAction func unwindToLogin(segue:UIStoryboardSegue) { }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: "userId") != nil {
            self.performSegue(withIdentifier: "showMainTabBar", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // is there a better place to put this?
        tableView.backgroundColor = UIColor.white
        tableView.contentInset = UIEdgeInsetsMake(150, 0, 0, 0)
        
        form +++ Section("")
            <<< EmailRow("Email") { row in
                row.title = "Email"
                row.placeholder = "Enter email"
            }
            <<< PasswordRow("Password") { row in
                row.title = "Password"
                row.placeholder = "Enter password"
            }
            <<< ButtonRow() { row in
                row.title = "Log In"
                row.onCellSelection(self.buttonSubmitTapped)
            }
//            <<< ButtonRow() { row in
//                row.title = "Forgot Password?"
//                row.onCellSelection(self.buttonForgotPasswordTapped)
//            }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var _ : UIViewController = segue.destination as UIViewController
    }
    
    func buttonSubmitTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        SVProgressHUD.show(withStatus: "Connecting...")
        
        let emailRow: EmailRow? = form.rowBy(tag: "Email")
        let passwordRow: PasswordRow? = form.rowBy(tag: "Password")

        let urlString = "https://murmuring-fortress-75115.herokuapp.com/customers/login"
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("hzHoUx04yGR4FBWZPcu9Kw", forHTTPHeaderField: "Authorization")
        request.httpBody = try? JSONSerialization.data(withJSONObject: ["email": emailRow!.value, "password": passwordRow!.value])

        let session = URLSession.shared

        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }

            let parsedResult: [String:AnyObject]!
            parsedResult = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject]

            if parsedResult["id"] != nil {
                DispatchQueue.main.sync { SVProgressHUD.dismiss() }
                UserDefaults.standard.set(parsedResult["id"], forKey: "userId")
                self.performSegue(withIdentifier: "showMainTabBar", sender: self)
            } else {
                DispatchQueue.main.sync { SVProgressHUD.dismiss() }
                let alert = UIAlertController(title: "Login Failed", message: "Email or Password were incorrect", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Try Again", comment: "Try Again"), style: .default))
                self.present(alert, animated: true, completion: nil)
            }
        }.resume()
    }
    
    func buttonForgotPasswordTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        print("tapped!")
    }

}

